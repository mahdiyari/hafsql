---
icon: file-code
---
# Examples

Some examples to make working with HafSQL easier.  

[:icon-link-external: Submit more examples](https://gitlab.com/mahdiyari/hafsql)
